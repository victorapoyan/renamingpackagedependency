package com.codecraft.library;


import android.util.Log;

import com.codecraft.library.rest.SoapClient;
import com.codecraft.library.rest.SoapInterface;
import com.codecraft.library.rest.models.ExchangeRequest;
import com.codecraft.library.rest.models.ExchangeResponse;
import com.codecraft.library.rest.models.Rate;

import java.util.List;

import retrofit.Callback;
import retrofit.Response;

public class SoapService {

    private final static String TAG = SoapService.class.getSimpleName();

    public static void printRates() {
        SoapClient
            .create(SoapInterface.class)
            .getLatestExchangeRates(new ExchangeRequest())
            .enqueue(new Callback<ExchangeResponse>() {
                @Override
                public void onResponse(Response<ExchangeResponse> response) {
                    final ExchangeResponse body = response.body();
                    if (body == null) {
                        Log.d(TAG, "----> Can't Get Rates Body.");
                        return;
                    }

                    Log.d(TAG, String.format("----> Rates for: %s", body.getResult().getCurrentDate()));
                    List<Rate> rates = body.getResult().getRates().getRates();
                    Log.d(TAG, String.format("Info About: %s Rates", rates.size()));
                    for (Rate rate: rates) {
                        Log.d(TAG, String.format("ISO: %s", rate.getISO()));
                        Log.d(TAG, String.format("Rate: %s", rate.getRate()));
                        Log.d(TAG, String.format("Amount: %d", rate.getAmount()));
                        Log.d(TAG, String.format("Difference: %s", rate.getDifference()));
                    }
                    Log.d(TAG, "<----");
                }

                @Override
                public void onFailure(Throwable throwable) {
                    Log.d(TAG, "----> Can't Get Rates: ", throwable);
                }
            });
    }
}