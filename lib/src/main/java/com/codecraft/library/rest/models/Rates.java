package com.codecraft.library.rest.models;


import org.simpleframework.xml.ElementList;

import java.util.List;

public class Rates {

    @ElementList(entry = "ExchangeRate", inline = true)
    private List<Rate> mExchangeRates;

    public List getRates() {
        return mExchangeRates;
    }
}
