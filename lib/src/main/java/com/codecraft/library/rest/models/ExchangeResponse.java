package com.codecraft.library.rest.models;


import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.NamespaceList;
import org.simpleframework.xml.Root;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

@Root(name = "soap12:Envelope")
@NamespaceList({
        @Namespace(reference = "http://www.w3.org/2001/XMLSchema-instance", prefix = "xsi"),
        @Namespace(reference = "http://www.w3.org/2001/XMLSchema", prefix = "xsd"),
        @Namespace(prefix = "soap12", reference = "http://www.w3.org/2003/05/soap-envelope")
})
public class ExchangeResponse {

    private final static DateFormat FORMAT =
            new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);

    @Element(name = "Body")
    @Namespace(prefix = "soap12")
    private Body mBody;


    static class Body {
        @Element(name = "ExchangeRatesLatestResponse")
        private Response mResponse;
    }

    static class Response {
        @Attribute(name = "xmlns", required = false)
        private String mXmlns;

        @Element(name = "ExchangeRatesLatestResult")
        private Result mResult;
    }

    public Result getResult() {
        return mBody.mResponse.mResult;
    }
}