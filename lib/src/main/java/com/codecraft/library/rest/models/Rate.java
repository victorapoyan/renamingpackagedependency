package com.codecraft.library.rest.models;


import org.simpleframework.xml.Element;

public class Rate {
    @Element(name = "ISO")
    private String mISO;

    @Element(name = "Amount", required = false)
    private Integer mAmount;

    @Element(name = "Rate")
    private Double mRate;

    @Element(name = "Difference", required = false)
    private String mDifference;

    public Rate() {

    }

    public Rate(String iso, Double rate) {
        mISO = iso;
        mRate = rate;
    }

    public String getISO() {
        return mISO;
    }

    public Integer getAmount() {
        return mAmount;
    }

    public Double getRate() {
        return mRate;
    }

    public String getDifference() {
        return mDifference;
    }
}
