package com.codecraft.library.rest.models;


import org.simpleframework.xml.Element;

public class Result {
    @Element(name = "CurrentDate", required = false)
    private String mCurrentDate;

    @Element(name = "NextAvailableDate", required = false)
    private String mNextAvailableDate;

    @Element(name = "PreviousAvailableDate", required = false)
    private String mPreviousAvailableDate;

    @Element(name = "Rates")
    private Rates mRates;

    public String getCurrentDate() {
        return mCurrentDate;
    }

    public Rates getRates() {
        return mRates;
    }
}
