package com.codecraft.library.rest;


import retrofit.Retrofit;
import retrofit.SimpleXmlConverterFactory;


public class SoapClient {

    private static final String BASEURL = "http://api.cba.am";

    public static <T> T create(final Class<T> clazz) {
        return new Retrofit.Builder()
                           .addConverterFactory(SimpleXmlConverterFactory.create())
                           .baseUrl(BASEURL)
                           .build()
                           .create(clazz);
    }
}