package com.codecraft.library.rest;


import com.codecraft.library.rest.models.ExchangeRequest;
import com.codecraft.library.rest.models.ExchangeResponse;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.Headers;
import retrofit.http.POST;

public interface SoapInterface {

    @Headers({
            "Content-Type: application/soap+xml",
            "Accept-Charset: utf-8"
    })
    @POST("exchangerates.asmx")
    Call<ExchangeResponse> getLatestExchangeRates(@Body ExchangeRequest request);
}